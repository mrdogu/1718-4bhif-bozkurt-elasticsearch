package com.bozkurt.elasticsearch;

import com.bozkurt.elasticsearch.model.Car;
import com.bozkurt.elasticsearch.model.Manufcature;
import com.bozkurt.elasticsearch.model.Offer;
import com.bozkurt.elasticsearch.model.User;
import com.bozkurt.elasticsearch.repository.CarRepo;
import com.bozkurt.elasticsearch.repository.ManufactureRepo;
import com.bozkurt.elasticsearch.repository.OfferRepo;
import com.bozkurt.elasticsearch.repository.UserRepo;
import org.apache.http.nio.protocol.Pipelined;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class DemoApplication {

    @Autowired
    public CarRepo carRepo;

    @Autowired
    public ManufactureRepo manufcatureRepo;

    @Autowired
    public OfferRepo offerRepo;

    @Autowired
    public UserRepo userRepo;

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

    @PostConstruct
    public void loadData(){
        Manufcature manufcature1 = new Manufcature();
        manufcature1.setAdress("Germany");
        manufcature1.setName("Porsche");

        Car car1 = new Car();
        car1.setBuild(2002);
        car1.setCylinder(8);
        car1.setMiles(20000);
        car1.setName("Panamera Turbo S");
        car1.setPs(400);
        car1.setManufcature(manufcature1);

        Car car2 = new Car();
        car2.setBuild(2018);
        car2.setCylinder(8);
        car2.setMiles(30000);
        car2.setName("911");
        car2.setPs(500);
        car2.setManufcature(manufcature1);

        Manufcature manufcature2 = new Manufcature();
        manufcature2.setAdress("Italy");
        manufcature2.setName("Maserati");

        Car car3 = new Car();
        car3.setBuild(2017);
        car3.setCylinder(8);
        car3.setMiles(20000);
        car3.setName("GranCabrio");
        car3.setPs(402);
        car3.setManufcature(manufcature1);

        Car car4 = new Car();
        car4.setBuild(2016);
        car4.setCylinder(8);
        car4.setMiles(30000);
        car4.setName("GranTurismo");
        car4.setPs(404);
        car4.setManufcature(manufcature1);

        manufcatureRepo.save(manufcature1);
        manufcatureRepo.save(manufcature2);
        carRepo.save(car1);
        carRepo.save(car2);
        carRepo.save(car3);
        carRepo.save(car4);

        User user1 = new User();
        user1.setFirstname("Dogukan");
        user1.setLastname("Bozkurt");
        user1.setPassword("insecurepassword");
        user1.setUsername("dogub");

        User user2 = new User();
        user1.setFirstname("Joachim");
        user1.setLastname("Grüneis");
        user1.setPassword("stillinsecurepassword");
        user1.setUsername("joachimg");

        userRepo.save(user1);
        userRepo.save(user2);

        Offer offer1 = new Offer();
        offer1.setCar(car1);
        offer1.setUser(user1);
        offer1.setPrice(120000099);

        Offer offer2 = new Offer();
        offer2.setCar(car2);
        offer2.setUser(user1);
        offer2.setPrice(120000099);

        Offer offer3 = new Offer();
        offer3.setCar(car3);
        offer3.setUser(user2);
        offer3.setPrice(120000099);

        Offer offer4 = new Offer();
        offer4.setCar(car4);
        offer4.setUser(user2);
        offer4.setPrice(120000099);

        offerRepo.save(offer1);
        offerRepo.save(offer2);
        offerRepo.save(offer3);
        offerRepo.save(offer4);
    }
}
