package com.bozkurt.elasticsearch.repository;

import com.bozkurt.elasticsearch.model.Offer;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface OfferRepo extends ElasticsearchRepository<Offer,Integer> {
}
