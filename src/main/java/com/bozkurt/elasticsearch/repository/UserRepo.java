package com.bozkurt.elasticsearch.repository;

import com.bozkurt.elasticsearch.model.User;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface UserRepo extends ElasticsearchRepository<User,Integer> {
}
