package com.bozkurt.elasticsearch.repository;

import com.bozkurt.elasticsearch.model.Car;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface CarRepo extends ElasticsearchRepository<Car,Integer> {
}
