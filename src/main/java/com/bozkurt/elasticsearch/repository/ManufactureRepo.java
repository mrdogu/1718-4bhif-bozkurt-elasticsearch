package com.bozkurt.elasticsearch.repository;

import com.bozkurt.elasticsearch.model.Manufcature;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

public interface ManufactureRepo extends ElasticsearchRepository<Manufcature,Integer> {
}
